# Chowpocket Checker

## Usage:
  * This Program is used to check if there is a 50% discount offer in www.chowpocket.com.
  * This program can also be used to monitor the current states of the offers in the said site.
 
## Installation:
  1. install requests, lxml, bs4 through pip
  2. run the following in the command line: python chowpocket_checker.py

## TO DO:
  * Add functionality where emails are sent to the list to notify
  * As of now everything is static, make things dynamic
