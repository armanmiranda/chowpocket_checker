import requests
from bs4 import BeautifulSoup
import time
import smtplib
import ast
import subprocess as sp

while True:
    print "Starting checking for discounts..."

    url = "http://www.chowpocket.com/buildings"
    headers = {'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/39.0.2171.95 Safari/537.36'}
    response = requests.get(url, headers=headers)
    soup = BeautifulSoup(response.text, "lxml")
    data = soup.find("div", {"class": "building-show-page"}).get("data-menus")
    data = ast.literal_eval(data)
    
    with_off = []

    for datum in data:
        if datum["count"] >= 23:
            with_off.append(datum["name"])
    
    if len(with_off) != 0:
        msg = "The following meals has the chance to go 50%% off: %r" % (with_off)
        fromaddr = "arman.miranda.ust@gmail.com"
        toaddrs = ["arman.miranda.ust@gmail.com"]

        print "From" + fromaddr
        print "To: " + str(toaddrs)
        print "Message: " + msg
        break
    else:
        tmp = sp.call('clear', shell=True)
        print "Seems like there's no appealing discounts at this moment."
        print "Here's the data we're looking at right now: " 
        
        for datum in data:
          print "\nName: %s\nCount: %s\nPrice: %s \n\n" % (datum["name"], datum["count"], datum["price"])
        time.sleep(60)
        print "1 min has passed, rechecking.."
        continue

